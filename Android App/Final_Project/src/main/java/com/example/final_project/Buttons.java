package com.example.final_project;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Properties;

public class Buttons extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttons);
        Bundle b = getIntent().getExtras();
       String IPAddressPort = "";
       String username = "";
       String password = "";
       String IPAddress = "";
       String Port = "";
       int portIndex = -1;
        if(b != null) {
            IPAddressPort = b.getString("IPAddress+Port");
            username = b.getString("username");
            password = b.getString("password");
        }
        //Toast.makeText(getApplicationContext(), IPAddress, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), username, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), password, Toast.LENGTH_SHORT).show();
        for(int i = 0; i < IPAddressPort.length(); i++)
        {
            if(IPAddressPort.charAt(i)==':') {
                IPAddress = IPAddressPort.substring(0, i);
                portIndex = i+1;
                break;
            }
        }
        Port = IPAddressPort.substring(portIndex,IPAddressPort.length());
        //Toast.makeText(getApplicationContext(), IPAddress, Toast.LENGTH_SHORT).show();
        //Toast.makeText(getApplicationContext(), Port, Toast.LENGTH_SHORT).show();
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        Button lock = findViewById(R.id.lock_button);
        final String finalPassword = password;
        final String finalIPAddress = IPAddress;
        final String finalPort = Port;
        final String finalUsername = username;
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //Toast.makeText(getApplicationContext(),"locked", Toast.LENGTH_SHORT).show();
                try {
                    Toast.makeText(getApplicationContext(),"locked", Toast.LENGTH_SHORT).show();
                   String exit = executeRemoteCommand(finalUsername, finalPassword, finalIPAddress, finalPort,'l');
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        Button unlock = findViewById(R.id.unlock_button);
        unlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               try {
                   Toast.makeText(getApplicationContext(),"unlocked", Toast.LENGTH_SHORT).show();
                   String exit = executeRemoteCommand(finalUsername, finalPassword, finalIPAddress, finalPort,'u');
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public static String executeRemoteCommand(String username,String password,String hostname,String port, char uOrl)
            throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(username, hostname, Integer.parseInt(port));
        session.setPassword(password);

        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        session.connect();

        // SSH Channel
        ChannelExec channelssh = (ChannelExec)
                session.openChannel("exec");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);

        // Execute command
        if(uOrl == 'u')
        //channelssh.setCommand("cd /home/pi/Downloads; python3 cmdlne_w_args.py u");
        channelssh.setCommand("cd /home/pi/Downloads; python3 cmdlne_w_args.py u");
        else
          //  channelssh.setCommand("cd /home/pi/Downloads; python3 cmdlne_w_args.py l");
        channelssh.setCommand("cd /home/pi/Downloads; python3 cmdlne_w_args.py l");
        channelssh.connect();
        channelssh.disconnect();
        return baos.toString();
    }


}
