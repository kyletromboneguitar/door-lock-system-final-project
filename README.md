# Door Lock System Final Project

Project from final year of Bachelor's degree in college. Contains the Android app source code and the Raspberry Pi code. The app will connect to the Raspberry Pi via SSH and execute the Python script to unlock and lock the system. Completed May 2019.