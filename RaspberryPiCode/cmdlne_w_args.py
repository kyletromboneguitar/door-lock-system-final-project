import sys
import RPi.GPIO as GPIO
import time
def turn(lock_state):
    GPIO.setwarnings(False)
    if lock_state == True:
        p.ChangeDutyCycle(7.5)
    else:
        p.ChangeDutyCycle(2.5)
    return
def LED(lock_state):
    GPIO.setwarnings(False)
    #GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
    #GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)
    if lock_state == False:
        GPIO.output(11, GPIO.HIGH) # Turn on
        return
    else:
        GPIO.output(11, GPIO.LOW) # Turn off
        return
GPIO.setmode(GPIO.BOARD)
#GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_UP)
p = GPIO.PWM(8, 50)

GPIO.setwarnings(False)
if sys.argv < 2:
   print('Too few arguments, please specify a lock state')
else:
    GPIO.setwarnings(False)
    #GPIO.setwarnings(False)
    #print(sys.argv[1])
    #print(sys.argv[0])
    lk = sys.argv[1]
    #print(lk)
#lk = 'u'
    f = open("test.txt",'r')
    str = f.read(1)
    f.close()
    if(str == 'l'):
        lock_state=True
        p.start(2.5)
    else:
        lock_state=False
        p.start(7.5)
    if(lock_state == False):
        GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)
    else:
        GPIO.setup(11, GPIO.OUT, initial=GPIO.HIGH)
    if(lk != str):
        turn(lock_state)
        LED(lock_state)
        with open("test.txt",'w') as f:
            if(lk=='l'):
                f.write("l\n")
            else:
                f.write("u\n")
            f.close()
